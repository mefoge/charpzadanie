CREATE TABLE [superhero] (
	id integer(10) NOT NULL,
	name varchar(50) NOT NULL,
	realname varchar(50) NOT NULL,
	brithplace varchar(100) NOT NULL,
  CONSTRAINT [PK_SUPERHERO] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [heropower] (
	id integer(10) NOT NULL,
	hero_id integer(10) NOT NULL,
	power_id integer(10) NOT NULL,
  CONSTRAINT [PK_HEROPOWER] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [power] (
	id binary NOT NULL,
	name varchar(255) NOT NULL,
	rate integer(2) NOT NULL,
  CONSTRAINT [PK_POWER] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO

ALTER TABLE [heropower] WITH CHECK ADD CONSTRAINT [heropower_fk0] FOREIGN KEY ([hero_id]) REFERENCES [superhero]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [heropower] CHECK CONSTRAINT [heropower_fk0]
GO
ALTER TABLE [heropower] WITH CHECK ADD CONSTRAINT [heropower_fk1] FOREIGN KEY ([power_id]) REFERENCES [power]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [heropower] CHECK CONSTRAINT [heropower_fk1]
GO