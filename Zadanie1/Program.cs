/* 
        Создайте консольное приложение на C# для определения - является ли введенная последовательность символов полиндромом.
        Предусмотреть возможность выбора режима работы приложения(согласно переданному аргументу командной строки при запуске):
 
                1. учитывать только числа и буквы(игнорируем пробелы, спецсимволы, знаки препинания и т.п.);
                2.учитывать все введенные символы.
*/
using System;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        class Palindrom
        {
            public static bool Palindrom1(string str)
            {
                Regex regex = new Regex(@"\w");
                MatchCollection matches = regex.Matches(str);
                string result = null;
                foreach (Match match in matches)
                    result += match.Value;
                return Palindrom2(result);
            }
            public static bool Palindrom2(string str)
            {
                int len = str.Length;
                for (int i = 0; i < len / 2; ++i)
                {
                    if (str[i] != str[len - i - 1])
                    {
                        return false;
                    }

                }
                return true;
            }
            static void Main()
            {
                int o = 0;
                Console.Write("Анализ палиндрома\n");
                Console.Write("\n1 - Учитывать только числа и буквы\n2 - Учитывать все введенные символы\n0 - Выход\n");
                Console.Write("\nВыберите пункт: ");
                o = Convert.ToInt32(Console.ReadLine());
                while (2 > 1)
                {
                    if (o > 2)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("\nВы ввели некорректный пункт!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("\n1 - Учитывать только числа и буквы\n2 - Учитывать все введенные символы\n0 - Выход\n");
                        Console.Write("\nВыберите пункт: ");
                        o = Convert.ToInt32(Console.ReadLine());
                    }                    
                    if (o == 0)
                    {
                        break;
                    }
                    if (o == 1)
                    {
                        Console.Write("\nВведите строку: ");
                        string str = Console.ReadLine();
                        if (Palindrom1(str))
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\nЭта строка - палиндром");
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("\nЭта строка - не палиндром");
                        }
                        Console.ForegroundColor = ConsoleColor.White; ;
                        Console.Write("\n1 - Учитывать только числа и буквы\n2 - Учитывать все введенные символы\n0 - Выход\n");
                        Console.Write("\nВыберите пункт: ");
                        o = Convert.ToInt32(Console.ReadLine());
                    }
                    if (o == 2)
                    {
                        Console.Write("\nВведите строку: ");
                        string str = Console.ReadLine();
                        if (Palindrom2(str))
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\nЭта строка - палиндром");
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("\nЭта строка - не палиндром");
                        }
                        Console.ForegroundColor = ConsoleColor.White;                          ;
                        Console.Write("\n1 - Учитывать только числа и буквы\n2 - Учитывать все введенные символы\n0 - Выход\n");
                        Console.Write("\nВыберите пункт: ");
                        o = Convert.ToInt32(Console.ReadLine());
                    }
                }
            }
        }
    }
}